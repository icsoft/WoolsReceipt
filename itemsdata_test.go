package main

func testdata() string {
	str := `
                                                                              Tax Invoice                                                                  Page 1 of 2

                                                                        ABN 88 000 014 675
                                                                                                               Invoice/Order Number: 36305117
                                                                                                               Transit Code: RBB10
                                                                                                               Date: 31 May 2019
                                                                                                               Delivery Time: 1500 - 1800

Delivery Instructions
Pls do not ring gate bell, but call 0413246985 for entry. I authorise my husband to receive the delivery if I am unavailable.
Supplied

     Line      Description                                                                                       Ordered        Supplied          Price    Amount
               * Collectables
      31       Woolworths earn & learn each                                                                          27             27           $0.00           $0.00
               Bakery
      1        * Golden waffles fluffy & delicious 5 pack                                                             1             1            $2.50           $2.50
      2        * Golden waffles light & buttery 8 pack                                                                1             1            $2.50           $2.50
      3        Woolworths bread wholemeal loaf 680g                                                                   2             2            $2.00           $4.00
               Biscuits & Snacks
      4        * Cc's corn chips original 175g                                                                        1             1            $1.65           $1.65
      7        * Cadbury freddo biscuit 167g                                                                          1             1            $1.50           $1.50
               Breakfast Foods
      5        Uncle tobys quick oats 500g                                                                            2             2            $2.00           $4.00
               Canned & Packet Food
      6        Dole pineapple premium tropical chunks 432g                                                            2             2            $1.00           $2.00
               Confectionery
      8        * Darrell lea fruitier & nuttier block 180g                                                            1             1            $3.00           $3.00
      9        * Darrell lea rocklea road block 180g                                                                  1             1            $3.00           $3.00
      10       * Mars bar 53g bar                                                                                     1             1            $0.89           $0.89
      11       * Mars snickers 50g bar                                                                                2             2            $0.89           $1.78
               Dairy
      12       Devondale colby cheese 625g                                                                            1             1            $6.50           $6.50
      13       Woolworths drought relief full cream milk 2l                                                           1             1            $2.20           $2.20
      14       Woolworths drought relief full cream milk 3l                                                           5             5            $3.30          $16.50
               Frozen Food
      15       Marathon spring rolls 4pk 640g                                                                         1             1            $2.60           $2.60
      16       Woolworths broccoli florets 500g                                                                       1             1            $2.50           $2.50
               Fruit & Vegetables
      17       Cavendish bananas each                                                                                20             20           $0.54          $10.80
      18       Fresh granny smith apples each                                                                         5             5            $0.79           $3.95
      19       Fresh tomato each                                                                                      5             5            $0.65           $3.25
      20       Mushrooms cups min. 200g                                                                               5             5            $1.60           $8.00
      21       Pear packham ripe & ready each                                                                        14             14           $0.67           $9.38
               Health & Wellbeing
      22       * Nature's way kids smart vita-gummies multivitamin 120 pack                                           2             2           $11.50          $23.00
               International Food
      23       Mission original tortillas 12 pack                                                                     1             1            $3.00           $3.00
               Meat
      24       Steggles family roast chicken whole 2kg - 3.1kg                                                        2                         $3.00/Kg
                ~ Steggles family roast chicken whole 2kg - 3.1kg                                                               1 @ 2.53Kg      $3.00/Kg         $7.58
                ~ Steggles family roast chicken whole 2kg - 3.1kg                                                               1 @ 2.58Kg      $3.00/Kg         $7.73
      25       Woolworths aussie pork chipolata 600g                                                                  2             2            $5.00          $10.00
               Pet Care
      26       * Dine finest serve mixed selection in gravy wet cat food mini pouch 6 pack                            1             1            $3.50           $3.50
      27       * Dine finest serve with meat in gravy wet cat food mini pouch 6 pack                                  1             1            $3.50           $3.50
      28       * Dine finest serve with poultry selection in gravy wet cat food mini pouch 6                          1             1            $3.50           $3.50
               pack
               Toiletries
      29       * Polident denture adhesive cream fresh mint 60g                                                       1             1            $9.00           $9.00
      30       * Sensodyne toothpaste?sensitive teeth daily care extra fresh 110g                                     1             1            $9.20           $9.20

                                  Registered Office: Woolworths Group Limited t/a Woolworths Online, 1 Woolworths Way, Bella Vista NSW 2153

* Indicates GST applicable items
~ Indicates meat items of variable weight, sold within a weight range, where a refund is applicable.


                                                Need help? Visit the Help & Support page on our website, or contact us on 1800 000 610
                                                                             Tax Invoice                                                                 Page 2 of 2

                                                                       ABN 88 000 014 675
                                                                                                           Invoice/Order Number: 36305117
                                                                                                           Transit Code: RBB10
                                                                                                           Date: 31 May 2019
                                                                                                           Delivery Time: 1500 - 1800

                                                                                                                                           Sub Total:      $172.51
                                                                               Thank you for working with us towards a greener future. Reusable bags:        $1.00
                                                                                                                                         Delivery Fee:       $0.00
                                                                                                                                        Invoice Total:     $173.51
                                                                                                                       Invoice total includes GST of:        $6.33
                                                                                                                                    Paid by Coupon:          $7.50
                                                                                                                                   Paid by Gift Card:      $169.30
                                                                                                                                        Paid Amount:       $176.80
                                                                                                                                     Refund Amount:          $3.29

Woolworths Rewards
You have earned 178 points.
4.0 cents per litre fuel saving has been added to your Woolworths Rewards card.




                                 Registered Office: Woolworths Group Limited t/a Woolworths Online, 1 Woolworths Way, Bella Vista NSW 2153

* Indicates GST applicable items
~ Indicates meat items of variable weight, sold within a weight range, where a refund is applicable.
  Your refund for these items is included in your Refund Amount.
                                              Need help? Visit the Help & Support page on our website, or contact us on 1800 000 610
`
	return str
}
