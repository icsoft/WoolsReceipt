package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type ProductItem struct {
	Amount      int    `json:"units"`           //String for JSON, the number of items or weight in grams
	Description string `json:"description"`     //The description of the item
	UnitPrice   int    `json:"priceunit"`       //String for JSON, The price per unit in cents
	AmtGrams    bool   `json:"amtgrams,string"` //indicates the Amount value is in grams
}

type ItemsTable struct {
	Items       []ProductItem `json:"items"`       //listing of all the products
	ReceiptDate string        `json:"receiptdate"` //date the order was made
	InvoiceNo   string        `json:"invoiceno"`   //name of the file used to get the data
}

func (w *ItemsTable) addItemProductItem(newItem *ProductItem) error {
	return w.addItem(newItem.Amount, newItem.Description, newItem.UnitPrice, newItem.AmtGrams)
}

//addItem Receives a new item in cents and amount in grams
func (w *ItemsTable) addItem(amount int, description string, unitPrice int, amtGrams bool) error {
	if amtGrams {
		if amount < 1 {
			return fmt.Errorf("Invalid amount, you need to have at least 1 gram. %d returned.", amount)
		}
	} else {
		if amount < 1 {
			return fmt.Errorf("Invalid amount, you need to have at least 1 item. %d returned.", amount)
		}
	}
	if unitPrice < 1 {
		return fmt.Errorf("Invalid value, product must be at least 1 cent. %d returned.", unitPrice)
	}

	w.Items = append(w.Items, ProductItem{amount, description, unitPrice, amtGrams})
	return nil
}

// testData loads test items instead of importing from the PDF
func ItemTestData() *ItemsTable {
	itemData := new(ItemsTable)

	itemData.addItem(1, "Food stuff number 1 food", 125, false)
	itemData.addItem(3, "Food stuff number 2 food", 185, false)
	itemData.addItem(1250, "Food stuff number 3 food", 495, true) //1.25Kg @ 4.95/Kg
	itemData.addItem(2, "Food stuff number 4 food", 85, false)
	itemData.addItem(1, "Food stuff number 5 food", 1215, false)
	itemData.ReceiptDate = time.Now().Format("02 Jan, 2006")
	rand.Seed(time.Now().UTC().UnixNano())

	itemData.InvoiceNo = fmt.Sprintf("testdata%d", rand.Intn(10000))

	return itemData
}

func ItemImportStringData(data string) (*ItemsTable, error) {

	itemData := new(ItemsTable)

	scanner := bufio.NewScanner(strings.NewReader(data))
	for scanner.Scan() {
		s := strings.TrimSpace(scanner.Text())
		if len(s) > 1 {
			log.WithField("TextLine", s).Debug("RAW")

			//get the invoice number
			if strings.HasPrefix(s, "Invoice/Order Number") {
				itemData.InvoiceNo = strings.TrimPrefix(s, "Invoice/Order Number: ")
				continue
			}

			//get the date
			if strings.HasPrefix(s, "Date:") {
				itemData.ReceiptDate = strings.TrimPrefix(s, "Date: ")
				continue
			}

			re := regexp.MustCompile(`[ ]{2,}`)
			result := re.Split(s, -1)
			log.Debugf("%#v", result)

			// Ignore the lines we don't need
			if len(result) < 4 {
				continue
			}

			//Ignore an item that doesnt has a dollar amount in the last column
			if !strings.ContainsRune(result[len(result)-1], '$') {
				continue
			} else {
				//If it does have a dollar amount it may be the $/Kg subheading, ignore that too.
				if strings.Contains(result[len(result)-1], "Kg") {
					continue
				}
			}

			//Get the total of the lineitem
			_, err := DollarParse(result[len(result)-1])
			if err != nil {
				log.WithField("TextLine", s).WithError(err).Error("Unable to parse total")
				continue
			}

			lineItem := new(ProductItem)

			//Get the per item price
			if strings.Contains(result[len(result)-2], "Kg") {
				//as the item price is per kilo and formatted like "$3.00/Kg", remove the "/Kg"
				lineItem.UnitPrice, err = DollarParse(strings.TrimSuffix(result[len(result)-2], "/Kg"))
				if err != nil {
					log.WithField("TextLine", s).WithError(err).Error("Unable to import line item data, error converting item price")
					continue
				}
				lineItem.AmtGrams = true
			} else {
				lineItem.UnitPrice, err = DollarParse(result[len(result)-2])
				if err != nil {
					log.WithField("TextLine", s).WithError(err).Error("Unable to import line item data, error converting item price")
					continue
				}
				lineItem.AmtGrams = false
			}

			//The number of items supplied or grams is supplied per Kg
			if lineItem.AmtGrams {
				//Remove superflous runes as grams will be something like "1 @ 2.53Kg"
				val := result[len(result)-3]
				val = val[strings.IndexRune(val, '@')+2:]
				val = strings.TrimSuffix(val, "Kg")
				fl, err := strconv.ParseFloat(val, 64)
				if err != nil {
					log.WithField("TextLine", s).WithError(err).Error("unable to import data, error converting the number of items")
					continue
				}
				lineItem.Amount = int((fl * 1000) + 0.5)
			} else {
				lineItem.Amount, err = strconv.Atoi(result[len(result)-3])
				if err != nil {
					log.WithField("TextLine", s).WithError(err).Error("unable to import data, error converting the number of items")
					continue
				}
			}

			// Now the description
			if lineItem.AmtGrams {
				lineItem.Description = result[0]
			} else {
				lineItem.Description = result[1]
			}

			err = itemData.addItemProductItem(lineItem)
			if err != nil {
				log.WithField("TextLine", s).WithError(err).Error("Adding Item")
				continue
			}
			log.Debugf("---Final List--- %v", lineItem)
		}
	}
	return itemData, nil
}

func ItemImportFileData(fileNamePath string) (*ItemsTable, error) {
	file, err := ioutil.ReadFile(fileNamePath)
	if err != nil {
		return new(ItemsTable), fmt.Errorf("unable to open the converted PDF, make sure you have converted it. Error: %v", err)
	}

	shoppingList := string(file)
	return ItemImportStringData(shoppingList)
}
