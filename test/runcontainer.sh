#!/bin/bash
set -e

function abort()
{
	echo "$@"
	exit 1
}

function cleanup()
{
	echo " --> Killing container"
	docker rm -f $ID >/dev/null
}

PWD=`pwd`

if [[ "$RUNTYPE" = "web" ]]; then
	echo " --> Starting insecure container called wools"
	ID=`docker run --name wools -d -p $LISTENPORT $NAME:$VERSION`
	sleep 5
	
	echo " --> Obtaining web port number"
	WEBPORT=`docker inspect --format='{{(index (index .NetworkSettings.Ports "$LISTENPORT/tcp") 0).HostPort}}' "$ID"`
	if [[ "$WEBPORT" = "" ]]; then
		abort "Unable to obtain container web port number"
	fi
	echo " --> Web port is $WEBPORT, starting Chromium"
	chromium-browser HTTP://127.0.0.1:$WEBPORT 2>&1 &
fi

if [[ "$RUNTYPE" = "ssh" || "$RUNTYPE" = "blind" ]]; then

	echo " --> Starting insecure container called wools"
	ID=`docker run --name wools -d -p 22 -p $LISTENPORT -v $PWD/test:/test $NAME:$VERSION /sbin/my_init --enable-insecure-key`
	sleep 5
	
	echo " --> Obtaining SSH port number"
	SSHPORT=`docker inspect --format='{{(index (index .NetworkSettings.Ports "22/tcp") 0).HostPort}}' "$ID"`
	if [[ "$SSHPORT" = "" ]]; then
		abort "Unable to obtain container SSH port number"
	fi
	
	trap cleanup EXIT
	
	echo " --> Enabling SSH in the container"
	docker exec -t -i $ID /etc/my_init.d/00_regen_ssh_host_keys.sh -f
	docker exec -t -i $ID rm /etc/service/sshd/down
	docker exec -t -i $ID sv start /etc/service/sshd
	sleep 2
fi

if [[ "$RUNTYPE" = "ssh" ]]; then
	echo " --> Logging into container"
	ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $PWD/test/insecure_key -p $SSHPORT root@127.0.0.1 
fi

if [[ "$RUNTYPE" = "blind" ]]; then
	echo " --> Logging into container and running tests"
	ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $PWD/test/insecure_key -p $SSHPORT root@127.0.0.1 /bin/bash /test/test.sh
fi