<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Icsoft, Software Development" />
		<meta name="author" content="Matthew Hooper" />
		<meta name="keywords" content="Icsoft, Software, Programming, Computers, Support" />
		
		<title>WoolsReceipt - Home</title>
		
		<link rel="shortcut icon" href="static/images/favicon.ico" />
		<link href="static/css/style.css" rel="stylesheet" />
		<script type="text/javascript" src="/static/scripts/items.js"></script>
		<script type="text/javascript">
            	var fn = "{{.InvoiceNo}}";
			var rd = "{{.ReceiptDate}}";
        </script>
	</head>
	<body>
		<div id="main">
			<a href="/"><img alt="Woolworths image" src="/static/images/woolworths.png"/></a>
			<h2>Receipt creation</h2>
			<p>Found {{len .Items}} line items for invoice {{.InvoiceNo}} on {{.ReceiptDate}} totalling {{grandTotal .Items}}</p> 
			Please select your items:
			{{/* List all line items from the PDF receipt */}}
			<div id="pdflist">
				 <table id="items">
					<tr>
						<th>Description</th>
						<th>Price/Unit</th>
						<th class="unitsth">Units</th>
						<th class="tick"></th>
					</tr>
					{{range .Items}}
					<tr>
						<td>{{.Description}}</td>
						<td>{{dollarString .UnitPrice}}{{ if .AmtGrams }}/Kg{{ end }}<input type="hidden" value="{{.UnitPrice}}"></td>
						<td class="units"><input type="number" value="{{decAmt .Amount .AmtGrams}}" step="{{decMinStep .AmtGrams}}" min="{{decMinStep .AmtGrams}}" max="{{decAmt .Amount .AmtGrams}}" id="amount">{{ if .AmtGrams }}Kg{{ end }}<input type="hidden" value="{{.AmtGrams}}"></td>
						<td class="tick"><input type="checkbox"></td>
					</tr>
					{{end}}
					<tr>
						<td><input type="text" id="newitem" value="" placeholder="New item description"></td>
						<td><input type="number" id="price" value="0.00" min="0.00" step="0.01"></td>
						<td class="units"><input type="number" id="amount" value="0" min="0" step="1"></td>
						<td><button onclick="addItem()">Add Item</button></td>
					</tr>
				</table> 
			</div>
			<input type="button" value="Submit" onclick="fillJSON()">
		</div>
	</body>
</html>