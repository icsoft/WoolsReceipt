<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Icsoft, Software Development" />
		<meta name="author" content="Matthew Hooper" />
		<meta name="keywords" content="Icsoft, Software, Programming, Computers, Support" />
		
		<title>WoolsReceipt - Error</title>
		
		<link rel="shortcut icon" href="static/images/favicon.ico" />
		<link href="static/css/style.css" rel="stylesheet" />
	</head>
	<body>
		<div id="main">
			<div id="graphic">
				Not a <a href="/"><img alt="Woolworths image" src="/static/images/woolworths.png"/></a> application
			</div>
			<h1>WoolsReceipt Error</h1>
			<h2>An application error has occurred</h2>
			<p>We applogise, but an error has occurred whilst processing your request.
			The server has returned the following information to help:</p>
			<div id="error">
				<h3>Error Number {{.HTMLErrorNo}}
				<p>{{.ErrorString}}</p></h3>
			</div>
			<div id="additional">
				<h4><u>Additional information</u></h4>
				{{.AdditionalInfo}}
			</div>
			<hr>
			Please try <a href={{.SuggestedUrl}}>{{.SuggestedUrlName}}</a> to continue.			
		</div>
	</body>
</html>