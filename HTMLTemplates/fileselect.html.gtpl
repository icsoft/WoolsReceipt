<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Icsoft, Software Development" />
		<meta name="author" content="Matthew Hooper" />
		<meta name="keywords" content="Icsoft, Software, Programming, Computers, Support" />
		
		<title>WoolsReceipt - Home</title>
		
		<link rel="shortcut icon" href="static/images/favicon.ico" />
		<link href="/static/css/style.css" rel="stylesheet" />
	</head>
	<body>
		<div id="main">
			This is NOT a <a href="https://www.woolworths.com.au/"><img alt="Woolworths image" src="/static/images/woolworths.png"/></a> application
			<p> This application is used to split your woolworths online receipts.</p>
			<h2>Receipt creation</h2>
			<fieldset>
				<legend>Upload File</legend>
				<form
				    enctype="multipart/form-data"
				    action="/upload"
				    method="post"
				   >
					PDF File: <input type="file" name="myFile" />
				    <button type="submit" name="upload" value="upload" />Upload File</button></br>
			    </form>
			</fieldset></br>
			Found {{.FileCount}} PDF files:
			<form action="/itemselect" method="post">
				<!-- List all pdf receipts from the server -->
				<div id="pdflist">
					 <table>
						<tr>
							<th>Filename</th>
							<th>Date</th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						{{range .Files}}
						<tr>
							<td>{{.Name}}.pdf</td>
							<td>{{.Date}}</td>
							<td><input type="submit" name="{{.Name}}" value="Select"></td>
							<td><a href="/usergenerated/fromwools/{{.Name}}.pdf" target="_blank">View</a></td>
							<td><input type="submit" name="delete{{.Name}}" value="Delete"></td>
						</tr>
						{{end}}
					</table> 
				</div>
				<input type="submit" name="refresh" value="Refresh">
        		</form>
				<p>Woolworths and the stylised W logo are trademarks of Woolworths Ltd and are in no way associated with Icsoft or any of its employees.</p>
				<div id="footer">WoolsReceipt programme {{.VersionInfo}}</div>
		</div>
	</body>
</html>
