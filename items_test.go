package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestImportStringData(t *testing.T) {

	itemData, err := ItemImportStringData(testdata())
	if err != nil {
		t.Errorf("importing string %s", err)
	}
	assert.Equal(t, 31, len(itemData.Items), "Total items")

}
